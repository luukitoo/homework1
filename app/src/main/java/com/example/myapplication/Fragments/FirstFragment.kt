package com.example.myapplication.Fragments

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.example.myapplication.R

class FirstFragment : Fragment(R.layout.fragment_first) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val textInput = view.findViewById<EditText>(R.id.textInput)
        val nextFragment = view.findViewById<Button>(R.id.nextFragment)

        val controller = Navigation.findNavController(view)

        nextFragment.setOnClickListener {

            val text = textInput.text.toString()

            if (text.isEmpty()) {
                return@setOnClickListener
            }

            val action = FirstFragmentDirections.actionLoginToAccount(text)
            controller.navigate(action)

        }

    }

}