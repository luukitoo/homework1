package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val controller = findNavController(R.id.fragmentContainer)
        val navigator = findViewById<BottomNavigationView>(R.id.navigationMenu)

        val AllFragments = setOf<Int>(R.id.login, R.id.account, R.id.image, R.id.creator)

        setupActionBarWithNavController(controller, AppBarConfiguration(AllFragments))
        navigator.setupWithNavController(controller)

    }
}